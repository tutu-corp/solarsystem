# Solar System
Solar system is an application to display page about planets

## General Info
This project is based on an Udemy tutorial about OOP.
I added switch on the color that change with the planets.

#### This project is created with :
* Swift 5
* IOS 16.4
* Xcode 14.0

## Screenshot
|Mercure|Terre|Uranus|Neptune|
|--|--|--|--|
|![Mercure](./captures/mercure.png)|![Terre](./captures/terre.png)|![Uranus](./captures/uranus.png)|![Neptune](./captures/neptune.png)|