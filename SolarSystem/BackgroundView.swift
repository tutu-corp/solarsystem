//
//  BackgroundView.swift of project named SolarSystem
//
//  Created by Aurore D (@Tuwleep) on 22/05/2023.
//
//  

import SwiftUI

struct BackgroundView: View {
    
    var deepColor: String
    
    var body: some View {
        ZStack{
           Spacer()
        }
        .background(
        LinearGradient(
            colors: [Color("ColorSpace"), Color(deepColor)],
            startPoint: UnitPoint(x: 0, y: 0),
            endPoint: UnitPoint(x: 1, y: 1.5))
        )
    }
}

struct BackgroundView_Previews: PreviewProvider {
    static var previews: some View {
        BackgroundView(deepColor: "Mercure")
    }
}
