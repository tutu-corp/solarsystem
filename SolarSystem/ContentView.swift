//
//  ContentView.swift of project named SolarSystem
//
//  Created by Aurore D (@Tuwleep) on 22/05/2023.
//
//  

import SwiftUI

struct ContentView: View {
    
    var planets = Datas().allPlanets
    @State var current = 0
    
    var body: some View {
        ZStack(alignment: .top){
            BackgroundView(deepColor: planets[current].planetColor())
            PlanetView(planet: planets[current])
            HStack {
                ButtonView(currentIndex: $current, forward: true)
                Spacer()
                ButtonView(currentIndex: $current, forward: false)
            }
            .padding()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
