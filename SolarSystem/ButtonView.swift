//
//  ButtonView.swift of project named SolarSystem
//
//  Created by Aurore D (@Tuwleep) on 22/05/2023.
//
//  

import SwiftUI

struct ButtonView: View {
    
    @Binding var currentIndex: Int
    var forward: Bool
    
    var body: some View {
        Button {
            forward ? moveForward() : moveBackwards()
        } label: {
            Image(systemName: forward ? "signpost.left.fill" : "signpost.right.fill")
                .font(.largeTitle)
                .foregroundColor(.white)
        }
    }
    
    func moveForward() {
        if currentIndex == 8 {
            currentIndex = 0
        } else {
            currentIndex += 1
        }
    }
    
    func moveBackwards() {
        if currentIndex == 0 {
            currentIndex = 8
        } else {
            currentIndex -= 1
        }
    }
}

struct ButtonView_Previews: PreviewProvider {
    static var previews: some View {
        ButtonView(currentIndex: .constant(0), forward: true)
    }
}
