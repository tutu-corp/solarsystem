//
//  SolarSystemApp.swift of project named SolarSystem
//
//  Created by Aurore D (@Tuwleep) on 22/05/2023.
//
//  

import SwiftUI

@main
struct SolarSystemApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
